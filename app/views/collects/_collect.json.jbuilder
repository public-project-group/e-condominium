json.extract! collect, :id, :current, :previous, :date, :created_at, :updated_at
json.url collect_url(collect, format: :json)
