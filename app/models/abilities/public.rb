module Abilities
	class Public
		include CanCan::Ability

		def initialize user
			can :read, Collect
			# can :read, Recommendation
			# can :read, Task
			# can :read, TaskClassify
			# can :read, Implementation
			# can :read, ImplementationClassify
			# can :historico, ManagementGroup
			# can :read, ExtendTime
			# can :read, PasfStatus
			# can :read, TaskExtend, task: { recommendation: { pasf: { pasf_status_id: PasfStatus.after_validade.pluck(:id) }}}
			# can :read, TaskExtendClassify	  	
		end
	end
end