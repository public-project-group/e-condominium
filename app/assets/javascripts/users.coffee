# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
	$('#user_cpf').mask("999.999.999-99");
	$('#user_phone').mask("(99) 9999-9999");
	$('#filtro_user_cpf').mask("999.999.999-99");
