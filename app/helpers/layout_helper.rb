module LayoutHelper
	def navbar_brand_image
		image_tag("logos/logo-condominium.jpeg", alt: t('app.title'))
	end
	def navbar_proceedings_list
		['proceedings']
	end
	def navbar_registers_list
		['registers', 'users', 'areas','signatures','pieces','type_pieces', 'documents', 'profiles','attachments']
	end
	def navbar_brand_image_min_right
		image_tag("logos/logo-condominium.jpeg", height: '50%', width: '50%', alt: t('app.title'))
	end
	def login_governo_image
		image_tag("logos/logo-condominium.jpeg", height: '100%', width: '80%', alt: t('app.title'))
	end
	def login_sistema_image
		image_tag("logos/logo-condominium.jpeg", height: '100%', width: '75%', alt: t('app.title'))
	end
end
