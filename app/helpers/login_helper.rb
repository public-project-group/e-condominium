module LoginHelper
	
	def image_epasf_logo
		image_tag 'logos/logo-condominium.jpeg' ,  class: 'img-fluid', width: '75%'
	end
	
	def image_cge_logo
		image_tag 'logos/logo-condominium.jpeg' , class: 'img-fluid hidden-xs-down mt-3', width: '45%'
	end

end