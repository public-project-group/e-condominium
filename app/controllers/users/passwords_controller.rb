# frozen_string_literal: true

class Users::PasswordsController < Devise::PasswordsController
  # GET /resource/password/new
  # def new
  #   super
  # end

  # POST /resource/password
  # def create
  #   super
  # end

  # GET /resource/password/edit?reset_password_token=abcdef

  def edit
    if reset_token_valid?(params[:reset_password_token])
      super  
    else
      flash[:alert] = t('devise.passwords.reset_token_invalid')
      redirect_to new_user_password_path
    end
  end

  # PUT /resource/password
  def update  
    if reset_token_valid?(params[:user][:reset_password_token])
      super  
    else
      flash[:alert] = t('devise.passwords.reset_token_invalid')
      redirect_to new_user_password_path
    end    
  end

  # protected

  # def after_resetting_password_path_for(resource)
  #   super(resource)
  # end

  # The path used after sending reset password instructions
  # def after_sending_reset_password_instructions_path_for(resource_name)
  #   super(resource_name)
  # end

  private

  def reset_token_valid?(reset_password_token)
    token_hash = Devise.token_generator.digest(User,:reset_password_token, reset_password_token)
    User.find_by({reset_password_token: token_hash}).present? and User.find_by({reset_password_token: token_hash}).reset_password_period_valid?
  end
end
