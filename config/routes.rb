Rails.application.routes.draw do

  resources :collects

  devise_for :users, path: 'auth', path_names: { sign_in: 'login', sign_out: 'logout' }
  
  resources :users

  root 'collects#index'

end