class CreateCollects < ActiveRecord::Migration[5.0]
  def change
    create_table :collects do |t|
      t.bigint :current
      t.bigint :previous
      t.date :date

      t.timestamps
    end
  end
end
