FactoryBot.define do
  factory :collect, class: Collect do
  	sequence(:current)                { |n| "#{n}#{Faker::Number.number(1)}"  } 
    sequence(:previous)               { |n| "#{n}#{Faker::Number.number(1)}"  }
  	#sequence(:spu)                 { |n| "#{n}#{Faker::Number.number(7)}"  }
  	nome                              { Faker::Name.name.upcase }
  	#cadastro_finalizado            { false }
  	#divida_tributaria              { false }
  	#cnpj                           { Faker::Number.number(14) }
  	#cpf                            { Faker::Number.number(11) }
  	#cgf                            { nil}
  	#tipo_receita_origem_debito_id  { 28 }
  	#numero_logradouro              { Faker::Number.number(4) }
    # anexo_processo                 { "test.pdf" }
    #complemento                    { Faker::Address.secondary_address }
 	  #telefone                       { "8588855555" } #Faker::Number.number(8) }
    #disposicoes_legais             { Faker::Lorem.sentence(3, true) }

    #after(:build) do |divida|
    #	divida.cep = FactoryBot.build(:cep)
    #	divida.debitos_divida << FactoryBot.build(:debito_divida)
    #end
  end
end

# current bigint,
#   previous bigint,
#   date date,